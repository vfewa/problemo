# Problemo

Problemo (PRAH-blay-MO)  
A database of problems for teams to solve.  

Created for the Elevate Hackathon 2018 at MaRS.
--------

To start the server:
npm start

If it errors on you, make sure you've updated node (v10.11.0) and npm (v6.4.1), then run
npm rebuild
npm start

--------

Team TODO
Define Schema
populate problem db
Get circles thing to work?!

--------
Notes:

Problem database    

As a user, I must be able to:

- create an account
- add my personal info
- add problem to db                     
- add data to a problem, industry tags
- search through problems
- ?? history of the problem, who was/is working on it
- ?? conversations about the problem - on another platform like reddit?

map of the problematique

Technical required parts:

- DB
- Server
- Frontend framework
- design framework - bootstrap
- viz components like https://bl.ocks.org/mbostock/7607535
